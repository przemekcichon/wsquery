<?php

class wsquery_response {

	protected $request;

	public function __construct( $country ){
		$this->request = 'http://api.owapro.hurra.com/tv/popular_spots?country=' . $country . '&json=1';
	}

	public function response() {
		$response = wp_remote_get( $this->request, array( 'timeout' => 240 ) );
		return $response;
	}

}

class wsquery_validate {

	protected $validated;
	protected $response;

	public function __construct( $country ){
		$this->response = new wsquery_response( $country );
	}

	public function validate() {
		$response = $this->response->response();
		try {
			$body = json_decode( $response['body'], true );
		} catch ( Exception $ex ) {
			$body = null;
		}
		if( $body ) {
			return $body;
		}
	}
} 

class wsquery_update_fields {

	protected $validated;
        
	public function __construct( $country ){
		$this->validated = new wsquery_validate( $country );
        
        // If the Polylang plugin is active...
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		if (is_plugin_active('polylang/polylang.php')) {
            $tvtracking = get_page_by_path('owapro/tv-tracking', ARRAY_A, 'hurraproduct');
			if( $country == 'DE' ) {
                $this->update_fields( pll_get_post( $tvtracking['ID'], 'de') );
                $this->update_fields( pll_get_post( $tvtracking['ID'], 'en') );
            } elseif ( $country == 'PL' ) {
                $this->update_fields( $tvtracking['ID'] );
            }
		}        
        
		
	}

	public function update_fields( $postid ) {
		
		$validated = $this->validated->validate();
        update_sub_field( array('nemesis_acf_tvposts_spots', 1, 'nemesis_acf_tvspots_filename' ), $validated[0]['spot_filename'], $postid);
        update_sub_field( array('nemesis_acf_tvposts_spots', 1, 'nemesis_acf_tvspots_spot_length' ), $validated[0]['spot_length'], $postid);
        update_sub_field( array('nemesis_acf_tvposts_spots', 1, 'nemesis_acf_tvspots_spot_times_shown' ), $validated[0]['spot_total_count'], $postid);
        update_sub_field( array('nemesis_acf_tvposts_spots', 2, 'nemesis_acf_tvspots_filename' ), $validated[1]['spot_filename'], $postid);
        update_sub_field( array('nemesis_acf_tvposts_spots', 2, 'nemesis_acf_tvspots_spot_length' ), $validated[1]['spot_length'], $postid);
        update_sub_field( array('nemesis_acf_tvposts_spots', 2, 'nemesis_acf_tvspots_spot_times_shown' ), $validated[1]['spot_total_count'], $postid);
        update_sub_field( array('nemesis_acf_tvposts_spots', 3, 'nemesis_acf_tvspots_filename' ), $validated[2]['spot_filename'], $postid);
        update_sub_field( array('nemesis_acf_tvposts_spots', 3, 'nemesis_acf_tvspots_spot_length' ), $validated[2]['spot_length'], $postid);
        update_sub_field( array('nemesis_acf_tvposts_spots', 3, 'nemesis_acf_tvspots_spot_times_shown' ), $validated[2]['spot_total_count'], $postid);
	}
}