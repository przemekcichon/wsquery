<?php
/**
 * Implements example command.
 */
class WSQuery_Command extends WP_CLI_Command {

    /**
     * Updates acf fields with data from webservice
     * 
     * 
     * ## EXAMPLES
     * 
     *     wp wsquery
     *
     */
    function __invoke() {
        new wsquery_update_fields( 'DE' );
        new wsquery_update_fields( 'PL' );
        add_action( 'plugins_loaded', array( $this, 'purge_url' ) );
    }
    public function purge_url(){
    	if ( function_exists( 'rocket_clean_files' ) ) {
            rocket_clean_files( 'http://tech.hurra.com/produkt/tv-tracking/' );
            rocket_clean_files( 'http://tech.hurra.com/produkt/tv-tracking-de/' );
        }
    }
}

WP_CLI::add_command( 'wsquery', 'WSQuery_Command' );
