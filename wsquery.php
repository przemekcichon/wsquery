<?php 

/*
Plugin Name: WS Query
Description: Webservice query
Version: 0.1
License: GPL 
Author: Hurra
Author URI: http://www.hurra.com/
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once( plugin_dir_path( __FILE__ ) . 'model.php' );

if ( defined('WP_CLI') && WP_CLI ) {
    include __DIR__ . '/wsquery-command.php';
}


